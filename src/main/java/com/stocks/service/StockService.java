package com.stocks.service;

import com.stocks.entity.Stock;
import com.stocks.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StockService implements Services {
    private StockRepository stockRepository;

    public StockService() {

    }

    @Autowired
    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public Iterable<Stock> findAll() {
        Iterable<Stock> stockIterable = stockRepository.findAll();
        return stockIterable;
    }

    @Override
    public Optional<Stock> findBySymbol(String symbol){
        return stockRepository.findBySymbol(symbol);
    }

    @Override
    public void deleteBySymbol(String symbol) {
         stockRepository.deleteStockBySymbol(symbol);
    }

    @Override
    public Stock save(Stock entity) {
        return stockRepository.save(entity);
    }
}
