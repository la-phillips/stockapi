package com.stocks.service;

import com.stocks.entity.Stock;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.Optional;

@Component
public interface Services {
    Iterable<Stock> findAll();
    Optional<Stock> findBySymbol(String symbol);
    void deleteBySymbol(String  symbol);
    Stock  save(Stock entity);

}
