package com.stocks.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Stock {

    @Id
    @GeneratedValue()
    private long id;

    @Column(name = "ticker", nullable = false, unique = true)
    private String symbol;
    private Date lastUpdatedAt;
    private double price ;

    public Stock(){

    }

    public Stock(String symbol, Date lastUpdatedAt, double price) {
        this.symbol = symbol;
        this.lastUpdatedAt = lastUpdatedAt;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Date getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Date lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", symbol='" + symbol + '\'' +
                ", lastUpdatedAt='" + lastUpdatedAt + '\'' +
                '}';
    }
}
