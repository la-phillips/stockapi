package com.stocks.repository;

import com.stocks.entity.Stock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends CrudRepository<Stock, String> {

    Optional<Stock> findBySymbol(String s);
    void deleteStockBySymbol(String s);

}
