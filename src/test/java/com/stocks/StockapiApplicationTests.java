package com.stocks;

import com.stocks.entity.Stock;
import com.stocks.service.Services;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockapiApplicationTests {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Services services;

    @Test
    public void contextLoads() {
    }

    @Test
    public void findBySymbol_SymbolPresent() {
        logger.info("{}", services.findBySymbol("gild").isPresent());
    }

    @Test
    public void findBySymbol_SymbolNotPresent() {
        assertFalse(services.findBySymbol("amzn").isPresent());
    }

    @Test
    public void saveStockToDB() {

        services.save(new Stock("mu", new Date(), 33.55));
        logger.info("{}", services.findBySymbol("mu").isPresent());
        assertTrue(services.findBySymbol("mu").isPresent());

    }

    @Test
    public void retrieveAllStocksFromDB(){
        logger.info("{}", services.findAll());
    }

    @Test
    public void deleteStockFromDB(){
        services.deleteBySymbol("mu");
        assertTrue(services.findBySymbol("mu").isPresent());
    }

    @Test
    public void updateStockPrice(){
        Optional<Stock> optionalStock = services.findBySymbol("fb");
        if(optionalStock.isPresent()){
            Stock stock = optionalStock.get();
            stock.setPrice(99);
            services.save(stock);
        }
        assertEquals(99.0,services.findBySymbol("fb").get().getPrice());
    }
}
